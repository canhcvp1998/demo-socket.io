/**
 * Questions?
 * 1. Can a client join multiple namespace
 * 2. Can a socket join multiple namespace
 * 3. Can a client can has multiple socket
 */

const io = require("socket.io");
const server = io(4000);

const defaultNamespace = "/";
const namespaceOne = "NSP1";
const namespaceTwo = "NSP2";

//#region Default namespace

// Default namespace "/"
server.on("connection", (socket) => {
  const namespace = socket.adapter.nsp.name;
  console.log(
    `[Namespace: ${namespace}]: New client connected with socket id: ${socket.id}`
  );

  socket.on("disconnect", () => {
    console.log(
      `[Namespace: ${namespace}]: Client with socket id: ${socket.id} has disconnected`
    );
  });
});

// Equivalent
server.of(defaultNamespace).on("connection", (socket) => {
  const namespace = socket.adapter.nsp.name;
  console.log(
    `[Namespace: ${namespace}]: New client connected with socket id: ${socket.id}`
  );

  socket.on("disconnect", () => {
    console.log(
      `[Namespace: ${namespace}]: Client with socket id: ${socket.id} has disconnected`
    );
  });
});

//#endregion Default namespace

const client = new Map();

//#region namespace one
server.of(namespaceOne).on("connection", (socket) => {
  const id = socket.handshake.query.id;
  client.set(id, socket.id);
  const namespace = socket.adapter.nsp.name;
  console.log(
    `[Namespace: ${namespace}]: New client connected with socket id: ${socket.id}`
  );

  socket.on("disconnect", () => {
    console.log(
      `[Namespace: ${namespace}]: Client with socket id: ${socket.id} has disconnected`
    );
  });
  socket.broadcast.emit("new client", socket.id);

  socket.on("pm2", (data) => {
    socket.to(client.get("2")).emit("pm2", data);
  });
});
//#endregion namespace one

//#region namespace two
server.of(namespaceTwo).on("connection", (socket) => {
  // socket.client.nsps: Map<namespace, Socket>
  const namespace = socket.adapter.nsp.name;
  console.log(
    `[Namespace: ${namespace}]: New client connected with socket id: ${socket.id}`
  );

  socket.on("disconnect", () => {
    console.log(
      `[Namespace: ${namespace}]: Client with socket id: ${socket.id} has disconnected`
    );
  });
});
//#endregion namespace two

//#region test

const namespaces = [...Array(10)].map((_, i) => `namespace ${i + 1}`);

const connectionHandler = (socket) => {
  const namespace = socket.adapter.nsp.name;
  console.log(
    `[Namespace: ${namespace}]: New client connected with socket id: ${socket.id}`
  );

  socket.on("disconnect", () => {
    console.log(
      `[Namespace: ${namespace}]: Client with socket id: ${socket.id} has disconnected`
    );
  });
};

namespaces.forEach((namespace) => {
  server.of(namespace).on("connection", connectionHandler);
});

//#endregion test
