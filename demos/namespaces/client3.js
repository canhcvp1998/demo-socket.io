const io = require("socket.io-client");

const socketUrl = "ws://localhost:4000";

const defaultNamespace = "";
const namespaceOne = "NSP1";
const namespaceTwo = "NSP2";

//#region Default namespace
const socketDefaultNamespace = io(`${socketUrl}/${defaultNamespace}`, {
  query: {
    id: 3,
  },
});

//#endregion Default namespace

//#region namespace one
const socketNamespaceOne = io(`${socketUrl}/${namespaceOne}`);

//#endregion namespace one

//#region namespace Two
const socketNamespaceTwo = io(`${socketUrl}/${namespaceTwo}`);

//#endregion namespace Two

//#region test
const namespaces = [...Array(10)].map((_, i) => `namespace ${i + 1}`);

namespaces.forEach((namespace) => {
  io(`${socketUrl}/${namespace}`);
});

//#endregion test

socketNamespaceOne.on("chat", (data) => {
  console.log(`client: ${data}`);
});

socketNamespaceOne.on("new client", (newSocketId) => {
  console.log(
    `[From client 3]: new client with id ${newSocketId} has joined namespace one`
  );
});

socketNamespaceOne.on("pm2", (data) => {
  console.log(`[From client 3 - pm2]: ${data}`);
});
