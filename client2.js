const io = require("socket.io-client");

const client = io("ws://localhost:4000");

const TOPIC = "training";
const payload = "nodejs";

const sendMessageToServer = () => {
  client.emit(TOPIC, payload);
};

// client.on("client", (data) => {
//   console.log("Client 2 received data: ", data);
// });

// client.on("new client", (data) => {
//   console.log("new client connected: ", data);
// });

// setInterval(() => {
//   sendMessageToServer();
// }, 1000);

client.on("message", (data) => {
  console.log("chat data in client 2: ", data);
});
// Turn off
// setTimeout(()=>{
//     client.close()
// }, 2000)
