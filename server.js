const io = require("socket.io");

const socket = io(4000);

const TOPIC_OF_CLIENT_2 = "training";
const TOPIC_CLIENT = "client";

socket.on("connection", (client) => {
  console.log(`new client with id`, client.id);
  // all client emit: broadcast
  // socket.sockets.emit("new client", client.id);

  // client.on("disconnect", (reason) => {
  //   console.log(
  //     `Client ${client.id} has disconnected as ${JSON.stringify(reason)}!`
  //   );
  // });

  // client.on(TOPIC_OF_CLIENT_2, (payload) => {
  //   console.log(`Received payload ${payload} of client ${client.id}`);
  // });

  // one client emit (one to one)
  // client.emit(TOPIC_CLIENT, client.id);

  client.on("chat", (data) => {
    client.broadcast.emit("message", data);
    // socket.sockets.emit("message", data);
  });
});
