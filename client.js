const io = require("socket.io-client")("ws://localhost:4000");

// io.on("client", (data) => {
//   console.log("Client 1 received data: ", data);
// });

// io.on("new client", (data) => {
//   console.log("new client connected: ", data);
// });

io.emit("chat", "client 2 oi!");

io.on("message", (data) => {
  console.log("chat data in client 1: ", data);
});
